#!/usr/bin/env bash

[ $# -ne 1 ] && exit 1
device=$1
[ -b "${device}" ] || exit 1

echo "=========================="
echo "basic system configuration"
echo myhost > /etc/hostname
ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime
sed -i -r 's/^#(fr_FR.UTF-8.*)/\1/' /etc/locale.gen
sed -i -r 's/^#(en_US.UTF-8.*)/\1/' /etc/locale.gen
locale-gen
echo export LANG=fr_FR.UTF-8 >> /etc/locale.conf
echo export LANGUAGE=C >> /etc/locale.conf
echo export LC_COLLATE=C >> /etc/locale.conf
echo KEYMAP=fr-latin9.map.gz >> /etc/vconsole.conf
echo FONT= >> /etc/vconsole.conf
echo FONT_MAP= >> /etc/vconsole.conf
echo "=========================="
echo

echo "============================="
echo "disk encryption configuration"
if [ ! -f /crypto_keyfile.bin ] ; then
  dd bs=512 count=4 if=/dev/urandom of=/crypto_keyfile.bin >/dev/null 2>&1
  chmod 000 /crypto_keyfile.bin
  echo "(you will be asked for encryption password to add keyfile)"
  cryptsetup luksAddKey ${device}p2 /crypto_keyfile.bin
fi
if grep "^HOOKS=.*encrypt" /etc/mkinitcpio.conf ; then
  echo -n
else
  sed -i -r 's/^(BINARIES=).*/\1(/usr/bin/btrfs /usr/bin/btrfsck)/' /etc/mkinitcpio.conf
  sed -i -r 's/^(HOOKS=.*filesystems ) *keyboard(.*)$/\1\2/' /etc/mkinitcpio.conf
  sed -i -r 's/^(HOOKS=.*)(filesystems.*)$/\1keyboard keymap encrypt resume \2/' /etc/mkinitcpio.conf
  sed -i -r 's/^(FILES=\()(.*\))$/\1\/crypto_keyfile.bin \2/' /etc/mkinitcpio.conf
  mkinitcpio -p linux
fi
echo "============================="
echo

echo "=================="
echo "systemd-boot configuration"
device_short=$(basename ${device})
bootctl --esp=/boot install
cat << EOF > /boot/loader/loader.conf
default arch.conf
editor yes
timeout 1
console-mode max
EOF
pacman -Sy intel-ucode # change me to amd-ucode if needed
PART_UUID=$(ls -l /dev/disk/by-uuid | grep ${device_short}p2$ | sed -r 's/^.* ([^ ]*) -> .*$/\1/')
cat << EOF > /boot/loader/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd intel-ucode.img
initrd /initramfs-linux.img
options cryptdevice=UUID=$PART_UUID:hdd:allow-discards root=/dev/mapper/vgsys-lvsys rootflags=subvol=/vol_rootfs rw
EOF
chmod -R g-rwx,o-rwx /boot
echo "=================="
echo
